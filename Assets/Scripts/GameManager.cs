using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Physics;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager GM;

    [SerializeField] private float xBound = 3f;
    [SerializeField] private float yBound = 3f;
    [SerializeField] private float ballSpeed = 3f;
    [SerializeField] private float respawnDelay = 2f;
    [SerializeField] private int[] playerScores;
    [SerializeField] private Text mainText;
    [SerializeField] private Text[] playerTexts;
    [SerializeField] private GameObject ballPrefab;

    private Entity ballEntityPrefab;
    private EntityManager manager;

    private WaitForSeconds oneSecond;
    private WaitForSeconds delay;

    private void Awake()
    {
        if (GM != null && GM != this)
        {
            Destroy(gameObject);
            return;
        }

        GM = this;

        playerScores = new int[2];
        oneSecond = new WaitForSeconds(1f);
        delay = new WaitForSeconds(respawnDelay);

        StartCoroutine(CountdownAndSpawnBall());
    }

    private void PlayerScored(int playerId)
    {
        playerScores[playerId]++;

        for (int i = 0; i < playerScores.Length && i < playerTexts.Length; i++)
            playerTexts[i].text = playerScores[i].ToString();

        StartCoroutine(CountdownAndSpawnBall());
    }

    IEnumerator CountdownAndSpawnBall()
    {
        mainText.text = "Get Ready";
        yield return delay;

        mainText.text = "3";
        yield return oneSecond;

        mainText.text = "2";
        yield return oneSecond;

        mainText.text = "1";
        yield return oneSecond;

        mainText.text = "";

        SpawnBall();
        
    }

    void SpawnBall()
    {

    }





}
